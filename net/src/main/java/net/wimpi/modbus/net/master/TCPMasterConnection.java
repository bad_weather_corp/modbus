/**
 *  Copyright 2017-2017 Anton Wiedermann
 *  Copyright 2002-2010 jamod development team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.wimpi.modbus.net.master;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.wimpi.modbus.Modbus;
import net.wimpi.modbus.io.ModbusTCPTransport;
import net.wimpi.modbus.io.ModbusTransport;

/**
 * Class that implements a TCPMasterConnection.
 *
 * @author Dieter Wimberger
 * @version 1.2 (@date@)
 */
public class TCPMasterConnection implements AutoCloseable {
   private final static Logger logger = LoggerFactory.getLogger("bwc.modbus.TCPMasterConnection");

   // instance attributes
   private Socket socket;
   private int timeout = Modbus.DEFAULT_TIMEOUT;
   private boolean connected;

   private final InetAddress address;
   private final int port;

   private ModbusTCPTransport modbusTransport;

   /**
    * Constructs a <tt>TCPMasterConnection</tt> instance with a given destination
    * address and default port
    *
    * @param adr
    *           the destination <tt>InetAddress</tt>.
    */
   public TCPMasterConnection(InetAddress adr) {
      this(adr, Modbus.DEFAULT_PORT);
   }

   /**
    * Constructs a <tt>TCPMasterConnection</tt> instance with a given destination
    * address.
    *
    * @param adr
    *           the destination InetAddress
    * @param port
    *           the destination port
    */
   public TCPMasterConnection(InetAddress adr, int port) {
      this.address = adr;
      this.port = port;
   }

   /**
    * create and establish new connection using default port
    * 
    * @param adr
    *           the destination InetAddress
    * @return
    * @throws IOException
    */
   public static TCPMasterConnection connect(InetAddress adr) throws IOException {
      return TCPMasterConnection.connect(adr, Modbus.DEFAULT_PORT);
   }

   /**
    * create and establish new connection
    * 
    * @param adr
    *           the destination InetAddress
    * @param port
    *           the destination port
    * @return
    * @throws IOException
    */
   public static TCPMasterConnection connect(InetAddress adr, int port) throws IOException {
      TCPMasterConnection conn = new TCPMasterConnection(adr, port);
      conn.connect();
      return conn;
   }

   /**
    * establish connection to the configured address and port
    *
    * @throws IOException
    *            if there is a network failure
    */
   public synchronized void connect() throws IOException {
      if (!connected) {
         logger.info("Connecting to {}:{}", address, port);
         socket = new Socket(address, port);
         setTimeout(timeout);
         prepareTransport();
         connected = true;
      } else {
         throw new IllegalStateException("Calling connect on already established connection");
      }
   }

   /**
    * close connection. Can be called on already closed connection
    */
   @Override
   public void close() {
      if (connected) {
         modbusTransport.close();
         connected = false;
      }
   }

   /**
    * Returns the ModbusTransport associated with this TCPMasterConnection
    *
    * @return the connection's ModbusTransport
    */
   public ModbusTransport getModbusTransport() {
      return modbusTransport;
   }

   /**
    * Prepares the associated ModbusTransport of this TCPMasterConnection for use
    *
    * @throws IOException
    *            if an I/O related error occurs.
    */
   private void prepareTransport() throws IOException {
      if (modbusTransport != null) {
         modbusTransport.close();
      }
      modbusTransport = new ModbusTCPTransport(socket);
   }

   /**
    * Returns the timeout for this <tt>TCPMasterConnection</tt>.
    *
    * @return the timeout as <tt>int</tt>.
    */
   public int getTimeout() {
      return timeout;
   }// getReceiveTimeout

   /**
    * Sets the timeout for this <tt>TCPMasterConnection</tt>.
    *
    * @param timeout
    *           the timeout as <tt>int</tt>.
    * @throws SocketException
    */
   public void setTimeout(int timeout) throws SocketException {
      this.timeout = timeout;
      if (socket != null) {
         socket.setSoTimeout(timeout);
      }
   }// setReceiveTimeout

   /**
    * Returns the destination port of this <tt>TCPMasterConnection</tt>.
    *
    * @return the port number as <tt>int</tt>.
    */
   public int getPort() {
      return port;
   }// getPort

   /**
    * Returns the destination <tt>InetAddress</tt> of this
    * <tt>TCPMasterConnection</tt>.
    *
    * @return the destination address as <tt>InetAddress</tt>.
    */
   public InetAddress getAddress() {
      return address;
   }// getAddress

   /**
    * Tests if this <tt>TCPMasterConnection</tt> is connected.
    *
    * @return <tt>true</tt> if connected, <tt>false</tt> otherwise.
    */
   public boolean isConnected() {
      return connected;
   }// isConnected

}// class TCPMasterConnection
