/**
 *  Copyright 2017-2017 Anton Wiedermann
 *  Copyright 2002-2010 jamod development team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.wimpi.modbus.io;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import net.wimpi.modbus.Modbus;
import net.wimpi.modbus.ModbusException;
import net.wimpi.modbus.ModbusSlaveException;
import net.wimpi.modbus.msg.ExceptionResponse;
import net.wimpi.modbus.msg.ModbusRequest;
import net.wimpi.modbus.msg.ModbusResponse;
import net.wimpi.modbus.net.master.TCPMasterConnection;

/**
 * Modbus over TCP transaction implementation.
 * 
 * NOTE that this implementation is not thread safe
 *
 * @author Dieter Wimberger
 * @version 1.2 (@date@)
 */
public class ModbusTCPTransaction implements ModbusTransaction {
   /** sequence to provide IDs for the transactions */
   private static AtomicInteger transactionSequence = new AtomicInteger(Modbus.DEFAULT_TRANSACTION_ID);

   // instance attributes and associations
   private TCPMasterConnection m_Connection;
   private ModbusTransport m_IO;
   private ModbusRequest m_Request;
   private ModbusResponse m_Response;
   private boolean m_ValidityCheck = Modbus.DEFAULT_VALIDITYCHECK;
   private boolean m_Reconnecting = Modbus.DEFAULT_RECONNECTING;
   private int m_Retries = Modbus.DEFAULT_RETRIES;

   /**
    * Constructs a new <tt>ModbusTCPTransaction</tt> instance.
    */
   public ModbusTCPTransaction() {
   }

   /**
    * Constructs a new <tt>ModbusTCPTransaction</tt> instance with a given
    * <tt>ModbusRequest</tt> to be send when the transaction is executed.
    * <p/>
    *
    * @param request
    *           a <tt>ModbusRequest</tt> instance.
    */
   public ModbusTCPTransaction(ModbusRequest request) {
      setRequest(request);
   }

   /**
    * Constructs a new <tt>ModbusTCPTransaction</tt> instance with a given
    * <tt>TCPMasterConnection</tt> to be used for transactions.
    * <p/>
    *
    * @param con
    *           a <tt>TCPMasterConnection</tt> instance.
    */
   public ModbusTCPTransaction(TCPMasterConnection con) {
      setConnection(con);
   }

   /**
    * Sets the connection on which this <tt>ModbusTransaction</tt> should be
    * executed.
    * <p>
    * An implementation should be able to handle open and closed connections.<br>
    * <p/>
    *
    * @param con
    *           a <tt>TCPMasterConnection</tt>.
    */
   public void setConnection(TCPMasterConnection con) {
      m_Connection = con;
      m_IO = con.getModbusTransport();
   }

   @Override
   public void setRequest(ModbusRequest req) {
      m_Request = req;
   }

   @Override
   public ModbusRequest getRequest() {
      return m_Request;
   }

   @Override
   public ModbusResponse getResponse() {
      return m_Response;
   }

   @Override
   public int getTransactionID() {
      return transactionSequence.get();
   }

   @Override
   public void setCheckingValidity(boolean b) {
      m_ValidityCheck = b;
   }

   @Override
   public boolean isCheckingValidity() {
      return m_ValidityCheck;
   }

   /**
    * Sets the flag that controls whether a connection is openend and closed for
    * <b>each</b> execution or not.
    * <p/>
    *
    * @param b
    *           true if reconnecting, false otherwise.
    */
   public void setReconnecting(boolean b) {
      m_Reconnecting = b;
   }

   /**
    * Tests if the connection will be openend and closed for <b>each</b> execution.
    * <p/>
    *
    * @return true if reconnecting, false otherwise.
    */
   public boolean isReconnecting() {
      return m_Reconnecting;
   }

   @Override
   public int getRetries() {
      return m_Retries;
   }

   @Override
   public void setRetries(int num) {
      m_Retries = num;
   }

   @Override
   public void execute() throws IOException, ModbusException {
      // check that the transaction can be executed
      if (m_Request == null || m_Connection == null) {
         throw new IllegalStateException("Assertion failed, transaction not executable");
      }
      // open the connection if not connected
      if (!m_Connection.isConnected()) {
         m_Connection.connect();
         m_IO = m_Connection.getModbusTransport();
      }

      // Retry transaction m_Retries times, in case of I/O Exception problems.
      int retryCounter = 0;
      while (retryCounter <= m_Retries) {
         try {
            // toggle and set the id
            m_Request.setTransactionID(transactionSequence.getAndIncrement());
            // 3. write request, and read response
            m_IO.writeMessage(m_Request);
            // read response message
            m_Response = m_IO.readResponse();
            break;
         } catch (IOException ex) {
            if (retryCounter >= m_Retries) {
               throw new IOException("Gave up after max retries: " + retryCounter, ex);
            } else {
               retryCounter++;
            }
         }
      }
      // deal with "application level" exceptions
      if (m_Response instanceof ExceptionResponse) {
         throw new ModbusSlaveException(((ExceptionResponse) m_Response).getExceptionCode());
      }
      // close connection if reconnecting
      if (isReconnecting()) {
         m_Connection.close();
      }
      // Check transaction validity
      if (isCheckingValidity()) {
         checkValidity();
      }
   }

   /**
    * Checks the validity of the transaction, by checking if the values of the
    * response correspond to the values of the request. Use an override to provide
    * some checks, this method will only return.
    *
    * @throws ModbusException
    *            if this transaction has not been valid.
    */
   protected void checkValidity() throws ModbusException {
   }

}
