/**
 *  Copyright 2017-2017 Anton Wiedermann
 *  Copyright 2002-2010 jamod development team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.wimpi.modbus.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.wimpi.modbus.Modbus;
import net.wimpi.modbus.msg.ModbusMessage;
import net.wimpi.modbus.msg.ModbusRequest;
import net.wimpi.modbus.msg.ModbusResponse;
import net.wimpi.modbus.util.ModbusUtil;
import net.wimpi.modbus.util.StreamUtils;

/**
 * Class that implements the Modbus transport flavor.
 *
 * @author Dieter Wimberger
 * @version 1.2 (@date@)
 */
public class ModbusTCPTransport implements ModbusTransport {
   private final static Logger logger = LoggerFactory.getLogger("bwc.modbus.ModbusTCPTransport");

   /** input stream */
   private final DataInputStream tcpInputStream;
   /** output stream */
   private final DataOutputStream tcpOutputStream;
   /** byte buffer */
   private final BytesInputStream bytesStream;

   /**
    * Constructs a new ModbusTransport instance for a given socket
    * 
    * @param socket
    *           the Socket used for data transport.
    */
   public ModbusTCPTransport(Socket socket) throws IOException {
      tcpInputStream = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
      tcpOutputStream = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
      bytesStream = new BytesInputStream(Modbus.MAX_IP_MESSAGE_LENGTH);
   }


   /* (non-Javadoc)
    * @see net.wimpi.modbus.io.ModbusTransport#close()
    */
   @Override
   public void close() {
      // close the streams
      StreamUtils.close(tcpInputStream);
      StreamUtils.close(tcpOutputStream);
      StreamUtils.close(bytesStream);
   }

   /* (non-Javadoc)
    * @see net.wimpi.modbus.io.ModbusTransport#writeMessage(net.wimpi.modbus.msg.ModbusMessage)
    */
   @Override
   public void writeMessage(ModbusMessage msg) throws IOException {
      msg.writeTo(tcpOutputStream);
      tcpOutputStream.flush();
   }


   /* (non-Javadoc)
    * @see net.wimpi.modbus.io.ModbusTransport#readRequest()
    */
   @Override
   public ModbusRequest readRequest() throws IOException {
      int functionCode = readMessageBytes(tcpInputStream, bytesStream);
      // create new instance of the request for given function code
      ModbusRequest req = ModbusRequest.createModbusRequest(functionCode);
      // read request contents from the buffer
      req.readFrom(bytesStream);
      logger.debug("Request read: {}", req);
      return req;
   }

   /* (non-Javadoc)
    * @see net.wimpi.modbus.io.ModbusTransport#readResponse()
    */
   @Override
   public ModbusResponse readResponse() throws IOException {
      int functionCode = readMessageBytes(tcpInputStream, bytesStream);
      // create response instance based on the function code
      ModbusResponse res = ModbusResponse.createModbusResponse(functionCode);
      // read response contents
      res.readFrom(bytesStream);
      logger.debug("Response read: {}", res);
      return res;
   }
   
   /**
    * read modbus message into buffer and return function code
    * 
    * @param is stream to read from
    * @param bytes bytes buffer to read data to
    * @return the function code
    * @throws IOException when read from the socket fails
    */
   private static int readMessageBytes(InputStream is, BytesInputStream bytes) throws IOException {
      // use same buffer
      byte[] buffer = bytes.getBuffer();
      // read the message header
      StreamUtils.readBytes(is, buffer, 0, 6);
      // extract length of the message
      int bf = ModbusUtil.registerToShort(buffer, 4);
      // read rest of the data
      StreamUtils.readBytes(is, buffer, 6, bf);
      // reset the byte buffer to indicate now-known length of the data
      int dataLength = 6 + bf;
      bytes.reset(buffer, dataLength);
      // skip 7 bytes (3*2+1) and read function code
      bytes.skip(7);
      int functionCode = bytes.readUnsignedByte();
      bytes.reset();
      // log information about read data
      logger.trace("Read {} bytes from the TCP stream representing message {}", dataLength, functionCode);
      // return the function code
      return functionCode;
   }
}
