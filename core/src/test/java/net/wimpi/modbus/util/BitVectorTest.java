/**
 *  Copyright 2017-2017 Anton Wiedermann
 *  Copyright 2002-2010 jamod development team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
/**
 * 
 */
package net.wimpi.modbus.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author wiedermanna
 *
 */
public class BitVectorTest {
  private BitVector bits;
  
  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
    bits = new BitVector(24);
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
    bits = null;
  }

  @Test
  public void testAccessModes() {
    // defaults
    assertTrue("default is LSB", bits.isLSBAccess());
    assertFalse("default is not MSB", bits.isMSBAccess());
    // toggle to MSB
    bits.toggleAccess();
    assertTrue("toggled to MSB", bits.isMSBAccess());
    assertFalse("toggled away from LSB", bits.isLSBAccess());
    // toggle back to LSB
    bits.toggleAccess();
    assertTrue("toggled to LSB", bits.isLSBAccess());
    assertFalse("toggled away from MSB", bits.isMSBAccess());
    // set the MSB
    bits.setMSBAccess(true);
    assertTrue("toggled to MSB", bits.isMSBAccess());
    assertFalse("toggled away from LSB", bits.isLSBAccess());
    bits.setMSBAccess(true);
    assertTrue("toggled to MSB", bits.isMSBAccess());
    assertFalse("toggled away from LSB", bits.isLSBAccess());
    // set LSB
    bits.setMSBAccess(false);
    assertTrue("toggled to LSB", bits.isLSBAccess());
    assertFalse("toggled away from MSB", bits.isMSBAccess());
    // set LSB
    bits.setMSBAccess(false);
    assertTrue("toggled to LSB", bits.isLSBAccess());
    assertFalse("toggled away from MSB", bits.isMSBAccess());
  }

  @Test
  public void testBitOperations() {
    // test initial values for bits
    assertEquals("Bit 0 was initially false", false, bits.getBit(0));
    assertEquals("Bit 7 was initially false", false, bits.getBit(7));
    assertEquals("Bit 10 was initially false", false, bits.getBit(10));
    bits.setBit(7, true);
    bits.setBit(10, true);
    assertEquals("Bit 0 is still false", false, bits.getBit(0));
    assertEquals("Bit 7 was set to true", true, bits.getBit(7));
    assertEquals("Bit 10 was set to true", true, bits.getBit(10));
    // test the set mode functionality
    bits.setMSBAccess(true);
    assertEquals("bit 13 is now what was 10 before", false, bits.getBit(10));
    assertEquals("bit 10 is now what was 13 before", true, bits.getBit(13));
    assertEquals("bit 7 is now what was 0 before", false, bits.getBit(7));
    assertEquals("bit 0 is now what was 7 before", true, bits.getBit(0));
    bits.setMSBAccess(false);
    assertEquals("bit 13 is now what was 10 before", true, bits.getBit(10));
    assertEquals("bit 10 is now what was 13 before", false, bits.getBit(13));
    assertEquals("bit 7 is now what was 0 before", true, bits.getBit(7));
    assertEquals("bit 0 is now what was 7 before", false, bits.getBit(0));
    // test the toggles
    bits.toggleAccess();
    assertEquals("bit 13 is now what was 10 before", false, bits.getBit(10));
    assertEquals("bit 10 is now what was 13 before", true, bits.getBit(13));
    assertEquals("bit 7 is now what was 0 before", false, bits.getBit(7));
    assertEquals("bit 0 is now what was 7 before", true, bits.getBit(0));
    bits.toggleAccess();
    assertEquals("bit 13 is now what was 10 before", true, bits.getBit(10));
    assertEquals("bit 10 is now what was 13 before", false, bits.getBit(13));
    assertEquals("bit 7 is now what was 0 before", true, bits.getBit(7));
    assertEquals("bit 0 is now what was 7 before", false, bits.getBit(0));
  }

}
