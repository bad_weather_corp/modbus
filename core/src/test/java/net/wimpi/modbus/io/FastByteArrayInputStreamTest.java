/**
 *  Copyright 2017-2017 Anton Wiedermann
 *  Copyright 2002-2010 jamod development team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.wimpi.modbus.io;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author wiedermanna
 *
 */
public class FastByteArrayInputStreamTest {

   /**
    * @throws java.lang.Exception
    */
   @Before
   public void setUp() throws Exception {
   }

   /**
    * @throws java.lang.Exception
    */
   @After
   public void tearDown() throws Exception {
   }

   @Test
   public void testReadSingleBytes() throws IOException {
      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      bos.write(5);
      bos.write(4);
      bos.write(3);
      bos.write(2);
      bos.write(1);
      bos.write(0);
      byte[] bytes = bos.toByteArray();
      assertEquals("check array size", 6, bytes.length);
      // create test instance
      try (FastByteArrayInputStream fbis = new FastByteArrayInputStream(bytes)) {
         assertEquals("read number 5", 5, fbis.read());
         assertEquals("read number 4", 4, fbis.read());
         assertEquals("read number 3", 3, fbis.read());
         assertEquals("read number 2", 2, fbis.read());
         assertEquals("read number 1", 1, fbis.read());
         assertEquals("read number 0", 0, fbis.read());
         assertEquals("read beyond the end", -1, fbis.read());
      }
   }

}
