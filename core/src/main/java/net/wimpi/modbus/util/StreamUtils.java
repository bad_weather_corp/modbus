/**
 *  Copyright 2017-2017 Anton Wiedermann
 *  Copyright 2002-2010 jamod development team
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.wimpi.modbus.util;

import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author wiedermanna
 *
 */
public class StreamUtils {
   private final static Logger logger = LoggerFactory.getLogger("bwc.modbus.StreamUtils");

   private StreamUtils() { /* utility class constructor */ }
   
   /**
    * Close the instance and log error if any
    * 
    * @param obj instance to be closed
    */
   public static void close(final Closeable obj) {
      try {
         obj.close();
      } catch (IOException e) {
         logger.warn("Failed to close {}", obj, e);
      }
   }
   
   /**
    * read required number of bytes or throw exception
    * 
    * @param in input stream to retrieve bytes from
    * @param buffer buffer to read data to
    * @param bufferOffset buffer offset
    * @param requestedBytes number of requested bytes
    * @throws IOException 
    * @throws EOFException 
    */
   public static void readBytes(final InputStream in, final byte[] buffer, final int bufferOffset, final int requestedBytes) throws IOException {
      // number of read bytes so far
      int totalBytes = 0;
      // number of bytes retrieved in last read
      int lastBytes;
      while (totalBytes < requestedBytes) {
         // read from the stream
         lastBytes = in.read(buffer, bufferOffset, requestedBytes-totalBytes);
         // if read returned -1 then we reached end of stream and failed to read the data
         if (lastBytes == -1) {
            throw new EOFException("Failed to read "+requestedBytes+ " from the stream");
         }
         // update counters
         totalBytes += lastBytes;
      }
   }
}
