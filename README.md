# Modbus for Java #

Modbus implementation for Java applications. This library is based off jamod (http://jamod.sourceforge.net).

### What is this repository for? ###

Library provides implementation of Modbus Protocol (https://en.wikipedia.org/wiki/Modbus)

### How do I get set up? ###

Checkout the repository and run "gradlew build" to assemble and test the library. Library is split into following parts

* core - the core implementation of the protocol and supporting classes
* net - support for TCP and UDP networks
* (not maintained) serial - support for serial communication - depends on rxtx library
* (not maintained) demo - demo implementation of the protocol use

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines
* Anyone willing to spend time with serial and demo modules is welcome :)

### Who do I talk to? ###

* use issue tracker for issues and feature requests